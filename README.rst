##############################
MPSM X-Axis Limit Switch Mount
##############################


Summary
=======

An adjustable mount for the X-axis limit switch of a Monoprice Select
Mini 3-D printer.

.. figure:: doc/images/x-axis-limit-switch-mount.jpeg
   :alt: Isometric CAD view of mount.

   Isometric CAD view of mount.



Details
=======

It is common to add a `1/8" thick borosilicate glass plate`_ on top of
the original equipment (OE) heated bed of the Monoprice Select Mini
3-D printer (MPSM).  The glass plate is flatter than the thin aluminum
sheet provided by the manufacturer, and it is generally easier to
remove prints from glass.

.. _1/8" thick borosilicate glass plate : https://www.mcmaster.com/#8476k62/=1cuaemn

The additional thickness of the glass plate exceeds the bed adjustment
screws' range, and the OE Z-axis limit switch provides no adjustment.
It is straightforward to move the mounting location of the Z-axis
switch with the help of parts_ found on Thingiverse.

.. _parts: https://www.thingiverse.com/thing:2217149

The new Z-axis limit switch mounting parts required the removal of the
decorative sheet metal shroud around the X-axis rails.  Unfortunately,
the X-axis limit switch was mounted to the shroud.  Several
suitable-looking X-axis limit switch mounts can be found on
Thingiverse, but none fit properly.  Hence, this design.

.. figure:: doc/images/mount_installed.jpeg
   :alt: X-axis limit switch mount , installed.

   X-axis limit switch mount , installed.

The mount is designed with Solvespace_, a free (GPLv3) parametric 3d CAD
tool.  An STL file generated from the Solvespace source files can be
found in the ``/build`` directory.

.. _Solvespace: http://solvespace.com/

.. figure:: doc/images/mount-dims_side.png
   :alt: Mount dimensions, side view.

   Mount dimensions, side view.

.. figure:: doc/images/mount-dims_top.png
   :alt: Mount dimensions, top view.

   Mount dimensions, top view.



Future Plans
============

Re-design in FreeCAD_.

.. _FreeCAD: https://www.freecad.org/



Copying
=======

Everything here is copyright © 2017-2023 Paul Mullen
(with obvious exceptions for any `fair use`_ of third-party content).

Everything here is offered under the terms of the MIT_ license.

    Basically, you can do whatever you want as long as you include the
    original copyright and license notice in any copy of the
    software/source.

.. _fair use: http://fairuse.stanford.edu/overview/fair-use/what-is-fair-use/
.. _MIT: https://tldrlegal.com/license/mit-license



Contact
=======

Feedback is (almost) always appreciated.  Send e-mail to
pm@nellump.net.
