# SLUG and RELEASE_VERSION are used in meta/install.txt, which can't
# abide whitespace!

# Should never change
NAME="MPSM X-Axis Limit Switch Mount"
SLUG="mpsm_x_axis_limit_switch_mount"
DESCRIPTION="An adjustable mount for the X-axis limit switch of a Monoprice Select Mini 3-D printer."
AUTHOR_NAME="Paul Mullen"
AUTHOR_CONTACT="pm@nellump.net"
COPYRIGHT_OWNER="Paul Mullen"
COPYRIGHT_CONTACT="pm@nellump.net"
LICENSE_SPDX_ID="MIT"

# May need updating
COPYRIGHT_YEARS="2017-2022"
RELEASE_VERSION="1.0.0"
RELEASE_DATE="2022-12-27"
